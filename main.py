import pickle
import random
from matplotlib import pyplot as plt
import numpy as np
from CNN import Network, Loss, TanH, ReLu, Gauss, Sigmoid
from mlxtend.data import loadlocal_mnist

train_images, train_labels = loadlocal_mnist(
    images_path=r'.\mnist\train-images.idx3-ubyte',
    labels_path=r'.\mnist\train-labels.idx1-ubyte')

evaluate_images, evaluate_labels = loadlocal_mnist(
    images_path=r'.\mnist\t10k-images.idx3-ubyte',
    labels_path=r'.\mnist\t10k-labels.idx1-ubyte')

train_images = train_images.reshape(len(train_images), 1, 28, 28)
train_images = train_images.astype('float32')
train_images /= 255

evaluate_images = evaluate_images.reshape(evaluate_images.shape[0],  1, 28, 28)
evaluate_images = evaluate_images.astype('float32')
evaluate_images /= 255



train_labels_clean = []
for i, label in enumerate(train_labels):
    out = [0 for y in range(10)]
    out[label] = 1
    train_labels_clean.append(out)
train_labels = train_labels_clean

evaluate_labels_for_check = evaluate_labels
evaluate_labels_clean = []
for i, label in enumerate(evaluate_labels):
    out = [0 for y in range(10)]
    out[label] = 1
    evaluate_labels_clean.append(out)
evaluate_labels = evaluate_labels_clean

network = Network(input_shape=  (1, 28, 28))
network.add_convolutional_layer(kernel_size=3, depth=64)
network.add_max_pooling_layer(3)
network.add_reshape_layer()
network.add_dense_layer(1000)
network.add_dense_layer(800)
network.add_dense_layer(10)


network.train(train_images, train_labels, evaluate_images, evaluate_labels, iterations=20, batch_size=128, test_size=10,
              learning_rate=0.01, metrics=True, loss_function=Loss.binary_cross_entropy)

evaluate_images_batch = []
evaluate_labels_batch = []
for i in range(10):
    index = random.randint(0, len(evaluate_labels) - 1)
    evaluate_images_batch.append(evaluate_images[index])
    evaluate_labels_batch.append(evaluate_labels[index])

for i, pic in enumerate(evaluate_images_batch):
    first_image = np.array(pic, dtype='float')
    pixels = first_image.reshape((28, 28))
    plt.imshow(pixels, cmap='gray')
    out = np.asarray(network.feedforward(pic))
    out = out.flatten()

    max_v = out[0]
    max_index = 0

    for i, v in enumerate(out):

        if v > max_v:
            max_v = v
            max_index = i

    print("NN-Out: " + str(max_index) + "  "+ str(max_v))
    plt.show()
