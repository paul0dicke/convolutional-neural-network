import numpy as np
np.seterr(all='raise')

class Loss:
    @staticmethod
    def binary_cross_entropy(target,prediction):
        target = np.asarray(target)

        return -1 * np.mean(target * np.log(prediction + 1e-50) + (1 - target) * np.log(1 - prediction + 1e-50))

    @staticmethod
    def binary_cross_entropy_prime(target, prediction):
        try:
            x = ((1 - target) / (1 - prediction) - target / prediction) / np.size(target)
        except FloatingPointError:
            return target - prediction
        return x