from CNN.layer.activationFunctions import ActivationFunction


class Layer(object):
    def __init__(self, input_shape: int, output_shape: int, activation_function: ActivationFunction):
        self._input_shape = input_shape
        self._output_shape = output_shape
        self._activation_function = activation_function


    @staticmethod
    def map(array, func):
        for i, c in enumerate(array):
            for j, _r in enumerate(c):
                array[i][j] = func(array[i][j])
        return array

    @property
    def input_shape(self):
        return self._input_shape

    @property
    def output_shape(self):
        return self._output_shape

    @output_shape.getter
    def output_shape(self):
        return self._output_shape

    @input_shape.getter
    def input_shape(self):
        return self._input_shape

    def feedforward(self, inputs) -> [float]:
        return

    def backpropagation(self, inputs: [float], outputs: [float], error: [float], learning_rate:float) -> [float]:
        pass
