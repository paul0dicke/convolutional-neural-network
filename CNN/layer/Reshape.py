import numpy as np
from CNN.layer.Layer import Layer


class Reshape(Layer):
    def __init__(self, input_shape: int):
        output_shape = 1
        for i in input_shape:
            output_shape *= i
        super(Reshape, self).__init__(input_shape, output_shape, None)

    def feedforward(self, inputs) -> [float]:

        return np.transpose(np.asmatrix(np.reshape(inputs, self.output_shape)))

    def backpropagation(self, inputs: [float], outputs: [float], error: [float], learningrate) -> [float]:
        return np.reshape(np.array(error), self.input_shape)
