from CNN.layer.activationFunctions import ActivationFunction
from CNN.layer.Layer import Layer
import numpy as np


class Dense(Layer):
    def __init__(self, input_shape: int, output_shape: int, activation_function: ActivationFunction, weights_min=-1,
                 weights_max=1,
                 bias_min=-1, bias_max=1):
        super(Dense, self).__init__(input_shape, output_shape, activation_function)

        self.weights_min = weights_min
        self.weights_max = weights_max
        self.bias_min = bias_min
        self.bias_max = bias_max
        self.biases = np.random.uniform(low=self.bias_min, high=self.bias_max, size=(self.output_shape, 1))

        self.weights = np.random.uniform(low=self.weights_min, high=self.weights_max,
                                         size=(self._output_shape, self._input_shape))

    def feedforward(self, inputs) -> [float]:

        inputs = np.matmul(self.weights.clip(-500,500), inputs.clip(-500,500)) + self.biases
        inputs = Layer.map(inputs, self._activation_function.activate)
        return inputs

    def backpropagation(self, inputs: [float], outputs: [float], error: [float], learning_rate: float) -> [float]:
        # calculate Gradient, the Errors * (derivative from Sigmoid(outputs))
        gradient = Layer.map(outputs, self._activation_function.deactivate)
        gradient = np.multiply(gradient, error, dtype=np.float64)
        gradient = np.multiply(gradient, learning_rate)

        self.biases += gradient

        # calculate delta + set weights: INPUT * gradient
        input_transposed = np.transpose(inputs)
        delta = np.matmul(gradient, input_transposed)
        self.weights += delta

        # calculate next error
        weights_transposed = np.transpose(self.weights)
        current_error = np.matmul(weights_transposed, error, dtype=np.float64)

        return current_error
