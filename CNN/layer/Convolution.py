from CNN.layer.activationFunctions import ActivationFunction
from scipy import signal

import numpy as np
from CNN.layer import Layer


class Convolution(Layer):
    def __init__(self, kernel_size: int, depth: int, input_shape, activation_function: ActivationFunction):
        self._input_shape = input_shape

        if type(input_shape) != tuple:
            raise TypeError("Input shape has to be a tupel!")

        if len(input_shape) != 3:
            raise ValueError("Input shape has to be 3 dimensional!")

        self._input_depth, self._input_height, self._input_width = input_shape

        self._kernel_depth = depth
        self._kernel_size = kernel_size

        self._kernel_shape = (depth, self._input_depth, kernel_size, kernel_size)
        self._output_shape = (depth, self._input_height - kernel_size + 1, self._input_width - kernel_size + 1)

        self._kernels = np.random.randn(*self._kernel_shape)
        self._biases = np.random.randn(*self._output_shape)

        self._outputs = self._output_shape

        self._activation_function = activation_function

    @property
    def output_shape(self):
        return self._output_shape

    def set_input_shape(self, input_shape: (int, int, int)):
        self._input_shape = input_shape
        self._input_depth, self._input_height, self._input_width = input_shape

        self._kernel_shape = (self._kernel_depth, self._input_depth, self._kernel_size, self._kernel_size)
        self._output_shape = (
            self._kernel_depth, self._input_height - self._kernel_size + 1, self._input_width - self._kernel_size + 1)

        self._kernels = np.random.randn(*self._kernel_shape)
        self._biases = np.random.randn(*self._output_shape)

        self._outputs = self._output_shape

    def feedforward(self, inputs: []):
        outputs = np.zeros(self._biases.shape)

        for kernel_depth in range(0, self._kernel_depth):
            for depth in range(0, self._input_depth):
                outputs[kernel_depth] += self._correlate(inputs[depth], self._kernels[kernel_depth][depth])

        return outputs

    def backpropagation(self, inputs: [float], output_gradient: [float], error: [float], learning_rate: float) -> [
        float]:
        
        kernels_gradient = np.zeros(self._kernels.shape)      

        for i in range(self._kernel_depth):
            for j in range(self._input_depth):
                
                
                output_gradient[i] = Layer.map(output_gradient[i], self._activation_function.deactivate)

                kernels_gradient[i, j] = signal.convolve(inputs[j], error[i], "valid")
                #input_gradient[i, j] += signal.convolve(np.rot90(self._kernels[i, j],2) ,output_gradient[i], "full").clip(-500, 500)
                
                #update kernels
                self._kernels[i, j] += learning_rate * kernels_gradient[i,j]
                self._biases[i, j] += learning_rate * output_gradient[i, j]
           
           

        return kernels_gradient

    def _correlate(self, inputs: [float, float], kernel: [float, float]):
        inputs = inputs.clip(-500,500)
        output = []
        for row in range(0, len(inputs) - self._kernel_size + 1):
            output_row = []
            for coll in range(0, len(inputs[row]) - self._kernel_size + 1):
                output_value = 0
                for kernel_row in range(0, self._kernel_size):
                    for kernel_coll in range(0, self._kernel_size):
                        output_value +=  self._activation_function.activate(inputs[row + kernel_row][coll + kernel_coll] * kernel[kernel_row][kernel_coll])

                output_row.append(output_value)
            output.append(output_row)

        return output
