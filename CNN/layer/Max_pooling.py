import math

import numpy as np

from CNN.layer import Layer


class Max_pooling(Layer):
    def __init__(self, input_shape: int, pool_size: int):

        output_shape = input_shape[0], math.ceil(input_shape[1] / pool_size), math.ceil(input_shape[2] / pool_size)

        super(Max_pooling, self).__init__(input_shape, output_shape, None)
        self.pool_size = pool_size
        self.max_values_index = []

    def feedforward(self, inputs) -> [float]:
        out = []
        for i, kernel in enumerate(inputs):
            out_kernel = []
            current_max_index = []
            for row in range(0, len(kernel), self.pool_size):
                out_kernel_row = []
                for coll in range(0, len(kernel[row]), self.pool_size):
                    pool = kernel[row:row + self.pool_size, coll:coll + self.pool_size]
                    out_kernel_row.append(pool.max())

                    # keep track of max indecees, need them for backpropagation
                    result = np.where(pool == np.amax(pool))
                    max_row = result[0][0]+ row
                    max_coll = result[1][0]+coll
                    current_max_index.append([max_row,max_coll])
                out_kernel.append(out_kernel_row)
            out.append(out_kernel)
            self.max_values_index.append(current_max_index)
        return np.asarray(out)

    def backpropagation(self, inputs: [float], outputs: [float], error: [float], learning_rate: float) -> [float]:
        out = np.zeros(self.input_shape)
        for i, kernels in enumerate(out):
            for j,err in enumerate(error[i]):

                out[self.max_values_index[i][j][0]][self.max_values_index[i][j][1]] = err[j]

        return out
