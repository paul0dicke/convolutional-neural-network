import math

import numpy as np


class ActivationFunction:
    @staticmethod
    def activate(x: float) -> float:
        pass

    @staticmethod
    def deactivate(y: float) -> float:
        pass


class Sigmoid(ActivationFunction):
    @staticmethod
    def activate(x: float) -> float:
        # to prevent overflow
        x = np.clip(x, -500, 500)
        return 1 / (1 + np.exp(-x))

    @staticmethod
    def deactivate(y: float) -> float:
        s = Sigmoid.activate(y)
        return s * (1 - s)


class ReLu(ActivationFunction):
    @staticmethod
    def activate(x: float) -> float:
        return max(0.0, x)

    @staticmethod
    def deactivate(y: float) -> float:
        return 0 if y < 0 else 1


class TanH(ActivationFunction):
    @staticmethod
    def activate(x: float) -> float:
        x = np.clip(x, -500, 500)
        return (math.exp(x) - math.exp(-x)) / (math.exp(x) + math.exp(-x))


    @staticmethod
    def deactivate(y: float) -> float:
        return 1 - TanH.activate(y) ** 2


class Gauss(ActivationFunction):
    @staticmethod
    def activate(x: float) -> float:
        return math.exp(-x ** 2)

    @staticmethod
    def deactivate(y: float) -> float:
        return -2 * y * math.exp(-y ** 2)
