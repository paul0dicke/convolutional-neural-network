"""Holds the Network Klass"""
from cProfile import label
import random
import sys
import time

from matplotlib import pyplot as plt
import numpy as np
from CNN import Layer, Dense, Convolution, Reshape, Loss, Max_pooling
from CNN.layer.activationFunctions.ActivationFunction import ActivationFunction, Sigmoid, ReLu


class Network:
    """The network"""

    def __init__(self, input_shape: int or tuple, learning_rate=0.1):

        self.mean_error = []
        self.mean_loss = []
        self._input_shape = input_shape

        self._layers: [Layer] = []

    def add_dense_layer(self, nodes: int, activation_function=Sigmoid, weights_min=-1, weights_max=1,
                        bias_min=-1, bias_max=1):
        if len(self._layers) == 0:
            denseLayer = Dense(self._input_shape, nodes, activation_function, weights_min, weights_max, bias_min,
                               bias_max)
        else:
            denseLayer = Dense(self._layers[-1].output_shape, nodes, activation_function, weights_min, weights_max,
                               bias_min,
                               bias_max)
        self._layers.append(denseLayer)

    def add_convolutional_layer(self, kernel_size: int, depth: int, activation_function = ReLu):
        if len(self._layers) == 0:
            convolutional_layer = Convolution(kernel_size, depth, self._input_shape, activation_function)
        else:
            convolutional_layer = Convolution(kernel_size, depth, self._layers[-1].output_shape, activation_function)

        self._layers.append(convolutional_layer)

    def add_reshape_layer(self):
        if len(self._layers) == 0:
            reshape_layer = Reshape(input)
        else:

            reshape_layer = Reshape(self._layers[-1].output_shape)

        self._layers.append(reshape_layer)

    def add_max_pooling_layer(self,pool_size:int):
        if len(self._layers) == 0:
            pool_layer = Max_pooling(input, pool_size)
        else:
            pool_layer = Max_pooling(self._layers[-1].output_shape, pool_size)
        self._layers.append(pool_layer)

    def feedforward(self, inputs) -> [float]:

        if type(self._layers[0]) == Dense:
            inputs = np.transpose(np.asmatrix(inputs))

            if self._input_shape != len(inputs):
                raise ValueError(
                    "Wrong input size for Net, expected: " + str(self._input_shape) + " got " + str(len(inputs)))

        else:
            if not isinstance(inputs, np.ndarray):
                raise TypeError("Expecting 'numpy.ndarray'")

            if self._input_shape != inputs.shape:
                raise ValueError(
                    "Wrong input size for Net, expected: " + str(self._input_shape) + " got " + str(inputs.shape))

        for layer in self._layers:
            inputs = layer.feedforward(inputs)

        return inputs

    def _feedforward_keep_hidden(self, inputs) -> [float]:

        if type(self._layers[0]) == Dense:
            inputs = np.transpose(np.asmatrix(inputs))

            if self._input_shape != len(inputs):
                raise ValueError(
                    "Wrong input size for Net, expected: " + str(self._input_shape) + " got " + str(len(inputs)))

        else:
            if not isinstance(inputs, np.ndarray):
                raise TypeError("Expecting 'numpy.ndarray'")

            if self._input_shape != inputs.shape:
                raise ValueError(
                    "Wrong input size for Net, expected: " + str(self._input_shape) + " got " + str(inputs.shape))
        hidden_inputs = [inputs]
        for layer in self._layers:
            inputs = layer.feedforward(inputs)
            hidden_inputs.append(inputs)

        return hidden_inputs

    def _backpropagation(self, inputs, targets, learning_rate: int):

        outputs = self._feedforward_keep_hidden(inputs)
        targets = np.transpose(np.asmatrix(targets))

        """Start back propagation"""
        # calc Errors for output
        error = np.subtract(targets, outputs[-1])
        
        for layer in reversed(self._layers):
            error = layer.backpropagation(outputs[-2], outputs[-1], error, learning_rate)
            outputs.pop()

    def train(self, inputs, targets, evaluate_inputs=None, evaluate_targets=None, iterations=5000, batch_size=100,
              test_size=10, learning_rate=0.1, loss_function=Loss.binary_cross_entropy_prime, metrics=False):

        for i in range(iterations):
            print("********************************")
            print("Running Batch " + str(i + 1) + "/" + str(iterations))

            train_data = random.sample(list(zip(inputs, targets)), batch_size)
            train_images_batch, train_labels_batch = zip(*train_data)

            evaluate_data = random.sample(list(zip(evaluate_inputs, evaluate_targets)), test_size)
            evaluate_images_batch, evaluate_labels_batch = zip(*evaluate_data)

            self._train_batch(train_images_batch, train_labels_batch, evaluate_images_batch, evaluate_labels_batch,
                              learning_rate, loss_function, metrics)

            sys.stdout.write("\r" + "Loss: " + str(self.mean_loss[-1]))
            
            print()

        self.show_mean_error()

    def _train_batch(self, inputs, target, evaluate_input, evaluate_target, learning_rate, error_function,
                     metrics=False):

        joined_lists = list(zip(inputs, target))
        random.shuffle(joined_lists)
        inputs, target = zip(*joined_lists)

        for index in range(len(inputs)):
            Network.print_progress("Training:", index, len(inputs))
            self._backpropagation(inputs[index], target[index], learning_rate)

        # Evaluate
        if metrics:
            joined_lists = list(zip(evaluate_input, evaluate_target))
            random.shuffle(joined_lists)
            evaluate_input, evaluate_target = zip(*joined_lists)
            loss = 0
            for index in range(len(evaluate_input)):
                Network.print_progress("Evaluate:", index, len(evaluate_input))
                out = self.feedforward(evaluate_input[index])
                out = np.asarray(out)
                loss += error_function(evaluate_target[index], out)
            loss = loss/len(evaluate_target)
            self.mean_loss.append(loss)
            

    def show_mean_error(self):
        plt.plot(range(len(self.mean_loss)), self.mean_loss, label= "loss")
        plt.xlabel("Epochs")
        plt.ylabel("mean absolute error")
        plt.legend(loc="upper left")
        plt.draw()  # pyplot draw()
        plt.show()

    @staticmethod
    def print_progress(msg, current, max):
        sys.stdout.write("\r" + msg + " " + str(round(current / max * 100)) + "%   ")
        sys.stdout.flush()
